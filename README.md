Java XMLReader / itwookie.com
-----

You can use the XMLReader class to parse any InputStream to a XMLNode or use a XMLRequest to get a XML file from the Internet.

The XMLReader is able to extract a specific node if you use `new XMLReader(String extractTag)`

Because the XMLRequest extends Thread you just have to call `.run()`. Then you can either poll for `isRunning()` and `getResult()`. As an alternative you can as well overwrite the `onRequestComplete(XMLNode result)` and `onRequestFailed(Throwable exception)` methods.    

If you're looking for a easy way to read XML documents on Android, just change the branch to `android` and download and import the files into your Android Project.

XMLNodes have a function to estimate the structures memory consumption. I wrote that code base on some articles I found online and I'm pretty sure it's not too accurate and maybe outdated, but it's still kind of interesting, so I left it in.

MIT License
-----

Copyright (c) 2016 ITWookie
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
