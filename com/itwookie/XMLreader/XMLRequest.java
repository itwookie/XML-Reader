package com.itwookie.XMLreader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class XMLRequest extends Thread implements Runnable {
	
	private URL target;
	private int timeout;
	private XMLNode res;
	private boolean runing = true;
	private String extract;
	private String userAgent="Java XML-Reader / itwookie.com";
	
	public XMLRequest(URL _target, int msTimeout) {
		target = _target;
		timeout = msTimeout;
		extract = null;
	}
	public XMLRequest(URL _target, int msTimeout, String extractNode) {
		target = _target;
		timeout = msTimeout;
		extract = extractNode;
	}
	
	@Override
	public void run() {
		
		InputStream is = null;
		XMLReader reader = null;
		Throwable exc=null;
		try {
			URLConnection con = target.openConnection();
			con.setRequestProperty("User-Agent", userAgent);
			con.setDoInput(true);
			con.setConnectTimeout(timeout);
			con.setReadTimeout(timeout);
			
			is = con.getInputStream();
			
			reader = new XMLReader(extract);
			
			res = reader.parse(is);

		} catch (IOException e) {
			exc = e;
		} finally {			
			if (reader != null) reader.clearReader();
			try { is.close(); } catch (IOException e) { }
		}
		runing = false;
		if (exc!=null) onRequestFailed(exc);
		else onRequestComplete(res);
	}
	
	public XMLNode getResult() {
		return res.clone();
	}
	public void clear() {
		res.delete();
		res = null;
	}
	public boolean isRunning() {
		return runing;
	}
	
	public void setUserAgent(String useragent) {
		userAgent = useragent;
	}
	
	public void onRequestComplete(XMLNode result) {
		
	}
	public void onRequestFailed(Throwable exception) {
		
	}
}
