package com.itwookie.XMLreader;

import java.util.ArrayList;
import java.util.List;

public class XMLUtils {
	
	public static String escapeXML(String arg0) {
		//Note the wrong order e6 is doing... for some reason ==> " -> &amp;quot; 
		String result = arg0;
		while (result.contains("&")) result=result.replaceAll("&", "%%amp%%");	//double to prevent endless loop
		while (result.contains("%%amp%%")) result=result.replaceAll("%%amp%%", "&amp;");
		while (result.contains("'")) result=result.replace("'", "&apos;");
		while (result.contains("\"")) result=result.replace("\"", "&quot;");
		while (result.contains(">")) result=result.replace(">", "&gt;");
		while (result.contains("<")) result=result.replace("<", "&lt;");
		return result;
	}
	
	public static String unescapeXML(String arg0) {
		String result = arg0;
		while (result.contains("&amp;")) result=result.replace("&amp;", "&");
		while (result.contains("&apos;")) result=result.replace("&apos;", "'");
		while (result.contains("&quot;")) result=result.replace("&quot;", "\"");
		while (result.contains("&gt;")) result=result.replace("&gt;", ">");
		while (result.contains("&lt;")) result=result.replace("&lt;", "<");
		return result;
	}
	
	public static List<String> listFromString(String arg0) {
		List<String> tmp = new ArrayList<String>();
		StringBuilder sb = new StringBuilder(); boolean inQuotes = false;
		arg0 = arg0.trim();
		if (arg0.length()<3) return tmp;
		arg0 = arg0.substring(1, arg0.length()-2).trim();
		for (int z = 0; z < arg0.length(); z++) {
			switch (arg0.charAt(z)) {
			case '"':
				inQuotes = !inQuotes;
				break;
			case ',':
				if (!inQuotes) {
					tmp.add(sb.toString().trim());
					sb = new StringBuilder();
					break;
				}
			default:
				sb.append(arg0.charAt(z));
			}
		}
		if (sb.length()>0) tmp.add(sb.toString().trim());
		return tmp;
	}
	public static boolean isStringArray(String arg0) {
		arg0 = arg0.trim();
		return (arg0.startsWith("[") && arg0.endsWith("]"));
	}
	/** checks if value is in list
	 * @param haystack - the escaped list like ["value", "value", "value"]
	 * @param needle - the value to look for
	 * @return true if contained
	 */
	public static boolean isInList(String haystack, String needle) {
		return listFromString(haystack).contains(needle);
	}
	
	public static String LC2S(List<Character> arg) {
		String res = "";
        if (arg == null || arg.isEmpty()) return "";
		for (Character c : arg) res += c;
		return res;
	}
	
	/*/public static Color hexColor(String hx) {
		if (!hx.matches("[#]{1}[0-9a-fA-F]{6}")) return null;
		return new Color(Integer.parseUnsignedInt(hx.substring(1), 16));
	}
	/*/
}
