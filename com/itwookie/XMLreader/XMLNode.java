package com.itwookie.XMLreader;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

@SuppressWarnings("serial")
public class XMLNode implements Cloneable, Serializable {
	private XMLNode parent;
	private List<XMLNode> children;
	private Map<String, String> attributes;
	private String type;
	private String content;
	
	public XMLNode(String _type, XMLNode _parent) {
		//super(_type, _parent);
		type = _type;
		parent = _parent;	//hoping this is only a reference
		children = new LinkedList<XMLNode>(); //Linked list would fill faster, ArrayList should search faster
		attributes = new HashMap<String, String>();
		content = "";
	}
	public XMLNode(String _type) {
		this(_type, null);
	}
	
	/** returns true if a attribute with the same key was replaced 
	 *	A value of null will delete the key mapping
	 */ 
	public boolean setAttribute(String key, String value) {
		String lk = key.toLowerCase();
		boolean ret = attributes.containsKey(lk);
		if (value == null && ret) attributes.remove(lk);
		else if (value != null) attributes.put(lk, value);
		return ret;
	}
	/** returns null if no such key exists */
	public Optional<String> getAttribute(String key) {
		String lk = key.toLowerCase();
		if (attributes.containsKey(lk))
			return Optional.of(attributes.get(lk));
		return Optional.empty();
	}
	public List<String> attributes() {
		List<String> unbound = new ArrayList<String>();
		if (!attributes.isEmpty()) 
			for (String s : attributes.keySet()) 
				unbound.add(s);
		return unbound;
	}
	/** if no key is specified all keys are copied */
	public void cloneAttributes(XMLNode from, String... keys) {
		if (from.attributes.size() == 0) return;
		if (keys.length == 0) {
			Set<String> tkeys = from.attributes.keySet();
			for (String key : tkeys) {
				if (from.attributes.containsKey(key))
					attributes.put(key, from.attributes.get(key));
			}
		} else {
			for (String key : keys) {
				if (from.attributes.containsKey(key))
					attributes.put(key, from.attributes.get(key));
			}
		}
	}
	/** arg0 is a string-array of attributes to remain in this node */
	public void clearAttributes(String... remain) {
		Set<String> keys = attributes.keySet(), keys2 = new HashSet<String>();
		for (String s : remain) keys2.add(s);
		keys.retainAll(keys2);
	}
	
	/** should not return null, but empty string if no text was contained */
	public String getContent() {
		return content;
	}
	public void appendContent(String text) { content = content + text; }
	public void setContent(String text) { content = text; }
    /** Return empty if element has no children (of tagName) */
    public Optional<String> getFirstChildContent(String tagName) {
        if (children!=null && !children.isEmpty()){
            List<XMLNode> children = getChildrenByTagName(tagName);
            if (!children.isEmpty()) {
                return Optional.of(children.get(0).getContent());
            }
        }
        return Optional.empty();
    }
	
	public String getType() { return type; }
	
	public XMLNode getNthChild(int index) { if (index < 0 || index > children.size()) throw new IndexOutOfBoundsException(); return children.get(index).clone(); }
    public List<XMLNode> getChildren() { return new LinkedList<XMLNode>(children); }
	/** returns a linked list containing all results one layer below this element */
	public List<XMLNode> getChildrenByAttributeValue(String attribute, String value) {
		List<XMLNode> result = new LinkedList<XMLNode>();
		if (attribute == null || value == null || attribute.isEmpty() || value.isEmpty()) return result;
		for (XMLNode c : children)
			if (value.equalsIgnoreCase(c.getAttribute(attribute).orElse(null)))
				result.add(c.clone());
		return result;
	}
	/** returns a linked list containing all results one layer below this element */
	public List<XMLNode> getChildrenByTagName(String type) {
		List<XMLNode> result = new LinkedList<XMLNode>();
		if (type == null || type.isEmpty()) return result;
		for (XMLNode c : children)
			if (type.equalsIgnoreCase(c.getType()))
				result.add(c.clone());
		return result;
	}
	/**overwrite existing children, so we are free to add without creating dupes.
	 * Note however, that equals for this class uses the type and md5 attribute
	 */
	public void addChild(XMLNode child) {
		int i = children.indexOf(child);
		if (i >= 0) {
			children.get(i).delete();
			children.set(i, child);
		} else 
			children.add(child);
	}
	public boolean deleteChild(XMLNode child) {
		int i = children.indexOf(child);
		if (i < 0) return false; 
		children.remove(i);
		return true;
	}
	public int getChildCount() {
		return children.size();
	}
	
	/** returns a linked list containing all results from all layers below this one */
	public List<XMLNode> getElementsByTagName(String type) {
		List<XMLNode> result = new LinkedList<XMLNode>();
		if (type == null || type.isEmpty()) return result;
		for (XMLNode c : children) {
			if (type.equalsIgnoreCase(c.getType()))
				result.add(c.clone());
			result.addAll(c.getElementsByTagName(type));
		}
		return result;
	}
	
	/** returns the parent node or NULL if no parent was given in the constructor
	 * Cloning a XML node will drop the parent and result in this returning NULL as well */
	public Optional<XMLNode> getParent() {
		return parent==null?Optional.empty():Optional.of(parent);
	}
	public boolean hasParent() {
		return parent != null;
	}

	/** Clone this node. This nodes parent node will be lost. Children will be reparented to this node */
	public XMLNode clone() {
		XMLNode clone = new XMLNode(type);
		Map<String, String> cloneattribs = new HashMap<String, String>();
		for (Entry<String, String> attrib : attributes.entrySet())
			cloneattribs.put(attrib.getKey(), attrib.getValue());
		clone.attributes = cloneattribs;
		clone.content = content;
		for (XMLNode child : children)
			clone.children.add(child.clone(clone));
		return clone;
	}
	
	/** Clone this node. This clone will refer to the argument as it's parent. Children will be reparented to this node */
	public XMLNode clone(XMLNode parent) {
		XMLNode clone = new XMLNode(type, parent);
		Map<String, String> cloneattribs = new HashMap<String, String>();
		for (Entry<String, String> attrib : attributes.entrySet())
			cloneattribs.put(attrib.getKey(), attrib.getValue());
		clone.attributes = cloneattribs;
		clone.content = content;
		for (XMLNode child : children)
			clone.children.add(child.clone(clone));
		return clone;
	}

	public String toString() {
		String ret;
		ret = "<" + type;
		if (!attributes.isEmpty()) for (Entry<String, String> attrib : attributes.entrySet())
			ret += " " + attrib.getKey() + "=\"" + attrib.getValue() + "\"";
		if (children.isEmpty() && content.isEmpty()) {
			ret += "/>\n";
		} else {
			ret += ">\n";
			if (content.isEmpty())
				for (XMLNode child : children)
					ret += child.toString("\t");
			else if (children.isEmpty())
				ret += "\t" + content + "\n";
			else if (!content.isEmpty() && !children.isEmpty())
				ret += "\t[[ THIS READER DOES NOT SUPPORT MIXED CONTENT - NODE CONTAINS CHILDREN AND TEXT ]]\n";
			ret += "</"+type+">\n";
			
		}
		return ret;
	}
	private String toString(String linePrefix) {
		String ret;
		ret = linePrefix + "<" + type;
		if (!attributes.isEmpty()) {
			for (Entry<String, String> attrib : attributes.entrySet())
				ret += " " + attrib.getKey() + "=\"" + attrib.getValue() + "\"";
		}
		if (children.isEmpty() && content.isEmpty()) {
			ret += "/>\n";
		} else {
			ret += ">\n";
			if (content.isEmpty())
				for (XMLNode child : children)
					ret += child.toString(linePrefix + "\t");
			else if (children.isEmpty())
				ret += linePrefix + "\t" + content + "\n";
			else if (!content.isEmpty() && !children.isEmpty())
				ret += linePrefix + "\t[[ THIS READER DOES NOT SUPPORT MIXED CONTENT - NODE CONTAINS CHILDREN AND TEXT ]]\n";
			ret += linePrefix + "</"+type+">\n";
		}
		return ret;
	}
	
	public void saveFile(OutputStreamWriter osw) throws IOException {
		osw.write("<" + type);
		if (!attributes.isEmpty()) {
			for (Entry<String, String> attrib : attributes.entrySet())
				osw.write(" " + XMLUtils.escapeXML(attrib.getKey()) + "=\"" + XMLUtils.escapeXML(attrib.getValue()) + "\"");
		}
		if (children.isEmpty() && content.isEmpty()) {
			osw.write("/>\n");
		} else {
			osw.write(">\n");
			if (content.isEmpty())
				for (XMLNode child : children)
					child.saveFile(osw, "\t");
			else if (children.isEmpty())
				osw.write("\t" + content + "\n");
			else if (!content.isEmpty() && !children.isEmpty())
				osw.write("\t[[ THIS READER DOES NOT SUPPORT MIXED CONTENT - NODE CONTAINS CHILDREN AND TEXT ]]\n");
			osw.write("</"+type+">\n");
		}
		
		osw.flush();
		osw.close();
	}
	private void saveFile(OutputStreamWriter osw, String linePrefix) throws IOException {
		osw.write(linePrefix + "<" + type);
		if (!attributes.isEmpty()) for (Entry<String, String> attrib : attributes.entrySet())
			osw.write(" " + XMLUtils.escapeXML(attrib.getKey()) + "=\"" + XMLUtils.escapeXML(attrib.getValue()) + "\"");
		if (children.isEmpty() && content.isEmpty()) {
			osw.write("/>\n");
		} else {
			osw.write(">\n");
			if (content.isEmpty())
				for (XMLNode child : children)
					child.saveFile(osw, linePrefix + "\t");
			else if (children.isEmpty())
				osw.write(linePrefix + "\t" + content + "\n");
			else if (!content.isEmpty() && !children.isEmpty())
				osw.write(linePrefix + "\t[[ THIS READER DOES NOT SUPPORT MIXED CONTENT - NODE CONTAINS CHILDREN AND TEXT ]]\n");
			osw.write(linePrefix + "</"+type+">\n");
		}
	}
	/*
	public void removeParent() {
		parent.delete();
		parent = null;
	}*/
	
	public void delete() {
		if (children != null && children.size() > 0)
			for (XMLNode child : children) {
				child.delete();
				child = null;
			}
		//if (parent != null) removeParent();
		if (attributes != null) { 
			attributes.clear();
			attributes = null;
		}
		this.type = null;
		this.content = null;
	}
	
	public long getEstimatedMemoryUsage() {
		long result = 8;	//jst for this instance
		
		result += 24;	//an array list would only use +4 here, but linked lists are faster  
		if (!children.isEmpty()) {
			for (XMLNode child : children) 
				result += child.getEstimatedMemoryUsage();
		}
		
		result += 32*attributes.size() + 4*(attributes.size()*0.75);	//map default base size
		for (Entry <String, String> attrib : attributes.entrySet()) {
			result += 8 * ((attrib.getKey().length() * 2) + 45) / 8;//minimum! not if substring
			result += 8 * ((attrib.getValue().length() * 2) + 45) / 8;
		}
		result += 8 * ((type.length() * 2) + 45) / 8;
		result += 8 * ((content.length() * 2) + 45) / 8;
		return result;
	}
	
	//*///
	public boolean equals(Object other) {
		if (other == null) return false;
		if (other.getClass() != XMLNode.class) return false;
		XMLNode that = (XMLNode) other;
        if (!type.equals(that.type)) return false;
        if (!content.equals(that.content)) return false;
        if (getChildCount() != that.getChildCount()) return false;
        if (attributes.size() != that.attributes.size()) return false;
        for (Entry<String,String> at : attributes.entrySet()) {
            if (!that.attributes.containsKey(at.getKey())) return false;
            if (!at.getValue().equals(that.attributes.get(at.getKey()))) return false;
        }
        if (getChildCount() > 0)
            for (int i = 0; i < getChildCount(); i++)
                if (!children.get(i).equals(that.children.get(i)))
                    return false;
        return true;
	}
	//*///
}
