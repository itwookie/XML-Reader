package com.itwookie.XMLreader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class XMLReader {
	private String EnclosingTag=null; private static boolean within=false;
	
	
	/** Read the whole stream */
	public XMLReader() {
		EnclosingTag = null;
	}
	/** Skip to the extractTag and read it. this tag should ocure only once */
	public XMLReader(String extractTag) {
		EnclosingTag = extractTag;
	}
	
	private XMLNode Nodes = null, Root = null;
	public void clearReader() {
		while (NodeUp()) ;
		if (Nodes != null) Nodes.delete();
		Nodes = null;
	}
	private String thrownText; //contains text between '>' and '<' to read at the closing brackets (trim!)

    int line=1;

	public XMLNode parse(InputStream xmlInputStream) {
		clearReader();
        line=1;
		
		BufferedReader is = new BufferedReader(new InputStreamReader(xmlInputStream), 512);
		
		String xmlTag;
		while ((xmlTag = readTag(is)) != null) {
			if ((xmlTag.startsWith("<?") && xmlTag.endsWith("?>")) || (xmlTag.startsWith("<!--") && xmlTag.endsWith("-->"))) continue;  //normally comments - no data
			if (xmlTag.startsWith("</") && xmlTag.endsWith("/>"))
				throw new RuntimeException("Closing tag can't be closed using />");
			if (xmlTag.startsWith("</")) {
				xmlTag = xmlTag.substring(2,xmlTag.length()-1).trim();
				if (xmlTag.contains(" "))
					throw new RuntimeException("Closingtag is not allowed to contain data (Contained space, around line " + line + ")");
				if (EnclosingTag != null) {
					if (within && EnclosingTag.equalsIgnoreCase(xmlTag))
						within=false;
					else
						continue;
				}
				if (!xmlTag.equalsIgnoreCase(Nodes.getType()))
					throw new RuntimeException("Invalid closing tag around line " + line + " </" + xmlTag.toUpperCase() + ">, </" + Nodes.getType().toUpperCase() + "> expected!");
				if (thrownText.length() > 0)
					Nodes.appendContent(XMLUtils.unescapeXML(thrownText));
				NodeUp();
			} else { // Startet mit <TAG
				String[] rawAttribs;
				rawAttribs = parseTag(xmlTag);
				if (rawAttribs.length%2 == 0) // durch <TAG wird die l�nge ungerade
					throw new RuntimeException("Invalid attributes length. Is there a key or value missing? (around line " + line + ") " + Arrays.toString(rawAttribs));
				if (EnclosingTag != null) {
					if (!xmlTag.endsWith("/>")) { //start
						if (!within && EnclosingTag.equalsIgnoreCase(rawAttribs[0]))
							within=true;
						else
							continue;
					} else { // end
						if (within && EnclosingTag.equalsIgnoreCase(rawAttribs[0]))
							within=false;
						else
							continue;
					}
				}
				XMLNode newNode = new XMLNode(rawAttribs[0], Nodes);
				for (int i = 1; i < rawAttribs.length; i+= 2)
					if (newNode.setAttribute(rawAttribs[i], rawAttribs[i+1]))
						throw new RuntimeException("XMLTag containes duplicated attribute key around line " + line);
				rawAttribs = null;
				
				if (Nodes == null)
					Nodes = Root = newNode;
				else {
					Nodes.addChild(newNode);
					if (!xmlTag.endsWith("/>"))
						Nodes = newNode;
				}
			}
			xmlTag = null;
		}
		
		if (Root == null) return null;
		
		return Root.clone();
	}
	
	/** returns null if eos was reached */
	private String readTag(BufferedReader is) {
		List<Character> res = new LinkedList<Character>();// StringBuilder res = new StringBuilder();
		List<Character> cont = new LinkedList<Character>();

		char[] c = new char[1]; int l;
		boolean inQuotes = false, exitwhile = false;
		thrownText = "";
		try {
			while ((l = is.read(c)) > 0) {
                //raw.add(c[0]);
                if (c[0] == 10) line++;
				if (c[0] == '<') {
					res.add(c[0]);
					break;
				} else {
					cont.add(c[0]);
				}
			}
			if (l < 1) return null;
			while ((l = is.read(c)) > 0) {
                //raw.add(c[0]);
                if (c[0] == 10) line++;
				res.add(c[0]);
				switch (c[0]) {
				case '"':
					inQuotes = !inQuotes;
					break;
				case '>':
					if (!inQuotes) exitwhile = true;
					break;
				}
				if (exitwhile) { exitwhile = false; break; }
			}
			if (l < 1) return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
        thrownText = XMLUtils.LC2S(cont).trim();
		return XMLUtils.LC2S(res) ;
	}

	/** should return [tag, key, value, key, value, ...] */
	private static String[] parseTag(String tag) {
		List<String> res = new LinkedList<String>(); 
		int i = 1; boolean inQuotes = false;		//init i with 1 cuz < will be skipped
		List<Character> elem = new LinkedList<Character>();/*StringBuilder elem = new StringBuilder();*/ char c;
		for (;i < tag.length(); i++) {
			c = tag.charAt(i);
			if (((c == ' ' || c == '=') && !inQuotes)) {
				if (elem.size()>0) {
					res.add(XMLUtils.LC2S(elem).trim());
					elem.clear(); //= new StringBuilder();
				}
			} else if (c == '"') {
				inQuotes = !inQuotes;
				if (!inQuotes /*&& elem.length()>0*/) { // "" muss auch gehen
					res.add(XMLUtils.LC2S(elem).trim());
					elem.clear(); //= new StringBuilder();
				}
			} else if ((c == '/' || c == '>') && !inQuotes) {
				break;
			} else {
				elem.add(c);
			}
		}
		if (elem.size()>0) res.add( XMLUtils.LC2S(elem).trim() );
		if (res.size()>1) for (int elno = 1; elno < res.size(); elno++) res.set(elno, XMLUtils.unescapeXML(res.get(elno)));
		return res.toArray(new String[res.size()]);
	}
	
	/** returns true when the root has been hit (will do nothing when true), false when moved one level up */
	private boolean NodeUp() {
		if (Nodes == null || !Nodes.hasParent()) return false;
		Nodes = Nodes.getParent().get();
		return true;
	}
}
